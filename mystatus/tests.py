from django.test import TestCase, Client
from django.urls import reverse, resolve
import time, datetime, pytz
from .models import MyStatus
from .forms import MyStatusForm
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class MyStatusTest(TestCase):
    #Functional Test
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(MyStatusTest, self).setUp()

    def tearDown(self):
        self.browser.close()
    
    def test_buat_status(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.browser.implicitly_wait(10)
        form = self.browser.find_element_by_id("status")
        submit = self.browser.find_element_by_id("submit")
        self.browser.implicitly_wait(10)
        form.send_keys("test")
        submit.click()
        self.assertIn("test", self.browser.page_source)

    #Unit Test
    def test_apakah_url_tersedia(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_menggunakan_fungsi_index(self):
        function = resolve('/')
        self.assertEqual(function.func, index)
    
    def test_apakah_models_bisa_dibuat(self):
        test_status = MyStatus.objects.create(status='Senang')
        jumlahobject = MyStatus.objects.all().count()
        self.assertEqual(jumlahobject, 1)
        self.assertEqual(str(test_status), 'Senang')
    
    def test_habis_post(self):
        response = self.client.post('/', {'status' : 'sedih' })
        self.assertEquals(response.status_code, 302)
