from django.shortcuts import render, redirect
from .forms import MyStatusForm
from .models import MyStatus
import datetime

def index(request):
    if request.method == "POST":
        form = MyStatusForm(request.POST)
        if form.is_valid():
            status = form.save(commit = False)
            status.time = datetime.datetime.now()
            status.save()
            return redirect('mystatus:index')
        
    else:
        form = MyStatusForm()
    all_status = MyStatus.objects.all().order_by("time").reverse()
    arg = {
        "form" : form,
        "objects" : all_status
    }
    return render(request, 'index.html', arg)