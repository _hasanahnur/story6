from django.db import models

class MyStatus(models.Model):
    status = models.CharField(max_length=300)
    time = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.status