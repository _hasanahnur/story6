from django import forms
from .models import MyStatus

class MyStatusForm(forms.ModelForm):
    class Meta:
        model = MyStatus
        fields = ["status"]

    def __init__(self, *args, **kwargs):
        super(MyStatusForm, self).__init__(*args, **kwargs)
        self.fields["status"].widget.attrs['id'] = "status"